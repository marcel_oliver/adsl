\documentclass[a4paper,11pt]{article}

% required packages
\usepackage[utf8x]{inputenc}
\usepackage{newtxtext,newtxmath}
\usepackage{paralist,html}
\usepackage{color}

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

%opening
\title{\textcolor{blue}{CA-MATH-810 Applied Dynamical Systems Lab}}
\author{Marc-Thorsten H\"utt and Marcel Oliver}
\date{Syllabus Fall 2021}

\begin{document}

\maketitle


\section{Course description}

Applied Dynamical Systems together with the associated Lab serves as a
first problem-oriented introduction to nonlinear dynamics. It also
provides the foundation for the third year specialization courses
\emph{Differential Equations} and \emph{Dynamical Systems and
Control}. Nonlinear phenomena will be explored both in the laboratory
and on the computer. The lab will cover real-world examples of
nonlinear dynamics experiments such as nonlinear electric oscillators
and pattern formation in chemical reactions, as well as some
paradigmatic models of nonlinear dynamics.

Programming environments will be Scientific Python for number
crunching and Mathematica for symbolic computing.

A main focus of the lab is the development of computational
experiments for nonlinear phenomena, the application of automated
tools for bifurcation analysis, and continuation methods.  We will
also implement simple agent-based models and pseudo-spectral PDE
solvers for reaction-diffusion equations.


\section{Grading}

The course grade is a portfolio grade derived from 
\begin{itemize}
\item 5 Code Assignments with weight 1 and
\item 5 Lab Reports with weight 2 each.
\end{itemize}
Participants should be prepared to present their report and/or program
code to the instructor or the entire class; this presentation may be
part of the grading scheme of a particular report or code assignment.

The three lowest-graded units of work are dropped from the grade
computation, so the grade is computed from 12 units in total.  This
rule covers all minor emergencies including minor illness and
extracurricular activities.  Further exceptions will only be made in
truly extenuating and well-documented circumstances.

Lab reports and code assignments are due within \textbf{one week} from
the day they are assigned (see ``Code'' resp.\ ``Report'') on the
course schedule below.

\section{Students registered for the 5 ECTS version}

Graduate students who take this class as part of their methods
education and who register for 5 ECTS only (course number
MMM008-110231) need to attend sessions from the schedule given below
and submit the associated assignments for at least 8 units and at most
10 units of work as defined above.  \emph{The first three code
submissions are mandatory.}

\section{Submission}

Code assignments and lab reports must be submitted via \textsf{git}
unless specified otherwise for a particular assignment.  Students are
required to set up \textsf{git} as described in the section ``Set up
steps for students'' in this document:
\begin{center}
  \url{https://bitbucket.org/marcel_oliver/git_for_academics}
\end{center}
The URL for the bitbucket course web page is 
\begin{center}
  \url{https://bitbucket.org/marcel\_oliver/adsl}
\end{center}
and the SSH URL which you will need to clone the repository is
\begin{center}
  \texttt{https://bitbucket.org/marcel\_oliver/adsl}
\end{center}

\section{Guidelines for code submissions}

You need to submit your code in one or more self-contained program
files so that the code can be run from the command line \emph{without
user intervention}, producing the requested output.  If you are asked
to answer short questions about the code or about the result, your
answers should be submitted in a separate file, or (if appropriate) as
source code comments in the program file.

Your code should be well-commented to explain the purpose of each
non-trivial step of your program.  The code structure must be clear;
variables and functions must be sensibly named.  Inadequately
commented or unnecessarily intransparent code will result in a lower
grade.

When submitting with git, place all files into the director which
contains the corresponding task sheet.  For Python assignments,
\emph{do not} use \textsf{Jupyter} notebooks as they cannot be easily
run from the command line and they cannot be diff'ed transparently.


\section{Guidelines for lab protocols}

Lab reports must be accompanied by all computer code used to generated
the results; see the section above for code submissions.

The report must be typeset in {\LaTeX } and submitted electronically
as a single PDF document; when submitting with git, the source file
must be submitted as well.
 
Reports should be as brief as possible (3--4 pages can sometimes be
enough) without omitting essential information or discussions.  You
don't need to write a text book!

The structure of the lab protocol resembles that of a short scientific paper: 
\begin{enumerate}
\item Title
\item Author
\item Abstract (briefly summarizing the goal and the result in 3-5 sentences)
\item Introduction (including a brief summary of the mathematical and
scientific background, as well as the history of the problem, if
applicable)
\item Methods (including details about the implementation, programming
strategy; in the ``Methods'' section you should describe exactly what
you have been doing: which algorithm, what parameters, experimental
setup, what postprocessing/visualization of the data, etc.)
\item Results 
\item Conclusion (summarizing your findings and embedding them in a broader perspective)
\item References
\end{enumerate}

All references, online or paper, must be included in the list of
references. All help received must be acknowledged, including code
segments taken from peers. There is no grade penalty for properly
credited help, as long as the submission as a whole remains in essence
an independent original contribution. Help or ``borrowed code''
without attribution is plagiarism and will be pursued according to the
Code of Academic Integrity.

\newpage

\section{Structure of the course (subject to change)}

\noindent
\begin{tabular}{llll}
\textbf{\textcolor{blue}{Date}}         &
\textbf{\textcolor{blue}{Instr.}}       &       
\textbf{\textcolor{blue}{Task}}         &       
\textbf{\textcolor{blue}{Topic}}        \\
Th, Sep 2  & MO  &        &      Introduction (git/Scipy) \\ 
Tu, Sep 7  & MTH &        &      Introduction II  \\ 
We, Sep 8  & MTH &        &      Introduction III \\ 
Th, Sep 9  & MTH &        &      Programming intro (Mathematica) \\ 
Tu, Sep 14 & MO  &        &      Logistic Map I \\
We, Sep 15 & MO  &        &      Logistic Map II \\
Th, Sep 16 & MO  & Code   &      Logistic Map III \\
Tu, Sep 21 & MTH &        &      Programming intro (Mathematica) \\ 
We, Sep 22 & MTH & Code   &      Programming intro (Mathematica) \\ 
Th, Sep 23 &     &        &      Homework session \\
Tu, Sep 28 & MO  &        &      ODE solvers I \\
We, Sep 29 & MO  &        &      ODE solvers II \\
Th, Sep 30 & MO	 & Code   &      ODE solvers III \\
Tu, Oct 5  & MTH &        &      Stability analysis of a simple neuron model I \\
We, Oct 6  & MTH &        &      Stability analysis of a simple neuron model II \\
Th, Oct 7  & MTH & Code   &      Basic principles of spiral wave formation \\ 
Tu, Oct 12 & MO  &        &      Chua's circuit: Theory \\ 
We, Oct 13 & MO  &        &      Chua's circuit: Computation \\ 
Th, Oct 14 & MO  &        &      Chua's circuit: Experiment (EE-Lab, to be re-scheduled!) \\ 
Tu, Oct 19 & MO  & Report &      Chua's circuit: Experiment (EE-Lab, to be re-scheduled!) \\ 
We, Oct 20 & MTH &        &      Biochemical switches and oscillators I \\ 
Th, Oct 21 & MTH &        &      Biochemical switches and oscillators II \\ 
Tu, Oct 26 & MTH & Report &      Biochemical switches and oscillators III \\ 
We, Oct 27 & 	 &        &      Homework session \\ 
Th, Oct 28 & MTH &        &      Cellular automata: Wolfram classes I \\ 
Tu, Nov 2  & MTH &        &      Cellular automata: Wolfram classes II \\
We, Nov 3  & MTH &        &      Cellular automata: Edge of chaos I \\
Th, Nov 4  & MTH & Code   &      Cellular automata: Edge of chaos II \\ 
Tu, Nov 9  & AM  &        &      Daisyworld I -- Biological homeostasis in an idealized world \\    
We, Nov 10 & AM  &        &      Daisyworld II \\ 
Th, Nov 11 & AM  & Report &      Daisyworld III \\ 
Tu, Nov 16 & MO  &	  &      Belousov--Zhabotinsky reaction I \\
We, Nov 17 & MO  &	  &      Belousov--Zhabotinsky reaction II \\
Th, Nov 18 & MO  & Report &      Belousov--Zhabotinsky reaction III \\ 
Tu, Nov 23 & TP  &        &      Finite difference schemes for 1D steady state diffusion \\
We, Nov 24 &     &        &      Homework session  \\ 
Th, Nov 25 & TP  &	  &      Diffusion equation \\ 
Tu, Nov 30 & TP  & Report &      Simple reaction-diffusion model \\ 
We, Dec 1  & MTH &        &      Excitable dynamics in networks \\ 
Th, Dec 2  & 	 &        &      Homework session
\end{tabular}


\end{document}
